<?php

$app->get('/api/users', FcmResearch\User\Action\FindAllUsers::class);

$app->put('/api/users/{user_id}', FcmResearch\User\Action\RegisterOrUpdateUser::class);

$app->put('/api/users/{user_id}/notification-id', FcmResearch\User\Action\SetNotificationId::class);

$app->delete('/api/users/{user_id}/notification-id', FcmResearch\User\Action\RemoveNotificationId::class);
