<?php

$app->get('/', FcmResearch\Notification\Action\ShowNotificationPage::class);
$app->post('/', FcmResearch\Notification\Action\PostNotificationPage::class)
    ->add(FcmResearch\Slim\Middleware\BaseUrlMiddleware::class);
