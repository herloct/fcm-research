<?php

return [
    Mustache_Engine::class => function ($c)
    {
        return new Mustache_Engine([
            'loader' => new Mustache_Loader_FilesystemLoader(__DIR__ . '/../templates')
        ]);
    }
];
