<?php

return [
    Predis\Client::class => function ($c)
    {
        $config = $c->get('config');

        return new Predis\Client($config['redis']);
    }
];
