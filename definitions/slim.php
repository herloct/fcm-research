<?php

return [
    'flash' => function ($c)
    {
        return new Slim\Flash\Messages();
    },

    Slim\Flash\Messages::class => function ($c)
    {
        return new Slim\Flash\Messages();
    },

    'foundHandler' => function ($c)
    {
        return new \Slim\Handlers\Strategies\RequestResponse();
    },

    'errorHandler' => function($c)
    {
        $maps = [
            Exception::class => [
                'http_code' => 500,
                'type' => 'unknown-error'
            ],
            FcmResearch\Exception\EntityNotFoundException::class => [
                'http_code' => function(FcmResearch\Exception\EntityNotFoundException $e)
                {
                    return $e->getCode();
                },
                'type' => function(FcmResearch\Exception\EntityNotFoundException $e)
                {
                    return $e->getType();
                }
            ],
            FcmResearch\Exception\ValidationException::class => [
                'http_code' => function(FcmResearch\Exception\ValidationException $e)
                {
                    return $e->getCode();
                },
                'type' => function(FcmResearch\Exception\ValidationException $e)
                {
                    return $e->getType();
                },
                'field' => function(FcmResearch\Exception\ValidationException $e)
                {
                    return $e->getField();
                }
            ]
        ];

        $config = $c->get('config');

        return new FcmResearch\Slim\Handler\Error($config['debug'], $maps);
    },

    FcmResearch\Slim\Middleware\BaseUrlMiddleware::class => function ($c)
    {
        $config = $c->get('config');
        return new FcmResearch\Slim\Middleware\BaseUrlMiddleware($config['base_url']);
    }
];
