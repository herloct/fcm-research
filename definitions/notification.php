<?php

return [
    FcmResearch\Notification\Service\SendFcmIndividual::class => function ($c)
    {
        $config = $c->get('config');

        $client = new GuzzleHttp\Client([
            'base_uri' => 'https://fcm.googleapis.com/fcm/send',
            'headers' => [
                'Authorization' => 'key='.$config['fcm.api_key']
            ]
        ]);

        return new FcmResearch\Notification\Service\SendFcmIndividual($client);
    }
];
