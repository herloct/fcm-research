<?php

return [
    'debug' => false,

    'base_url' => 'YOUR_BASE_URL',

    'redis' => [
        'host' => 'redis'
    ],

    'fcm' => [
        'api_key' => 'SOME_API_KEY'
    ]
];
