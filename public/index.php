<?php

session_start();

require __DIR__ . '/../vendor/autoload.php';

$config = new \Noodlehaus\Config([
    __DIR__ . '/../configs/config.php',
    sprintf('?%s/../configs/config.local.php', __DIR__)
]);

$app = new FcmResearch\Slim\App(__DIR__ . '/../definitions', $config);

$routes = glob(__DIR__ . '/../routes/*.php');
foreach ($routes as $route) {
    include $route;
}

$app->run();
