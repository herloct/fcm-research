<?php

namespace FcmResearch\User\Action;

use Cake\Chronos\Chronos;
use FcmResearch\Exception\ValidationException;
use FcmResearch\User\Repository\UserRepository;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class FindAllUsers
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function __invoke(Request $request, Response $response)
    {
        $users = $this->userRepository->findAll();

        return $response->withHeader('Content-Type', 'application/json')->write(json_encode([
            'data' => array_map(function ($user)
            {
                return [
                    'id' => $user->id,
                    'username' => $user->username,
                    'is_subscribed' => ! empty($user->notification_id),
                    'updated_at' => $user->updated_at
                ];
            }, $users),
            'total' => count($users)
        ]));
    }
}
