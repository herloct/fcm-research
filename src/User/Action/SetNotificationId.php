<?php

namespace FcmResearch\User\Action;

use Cake\Chronos\Chronos;
use FcmResearch\Exception\EntityNotFoundException;
use FcmResearch\User\Repository\UserRepository;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class SetNotificationId
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function __invoke(Request $request, Response $response)
    {
        $id = $request->getAttribute('user_id');
        $input = $request->getParsedBody()['data'];

        $userRepository = $this->userRepository;

        if (! $userRepository->isExists($id)) {
            throw new EntityNotFoundException('Cannot find User.');
        }

        $user = $userRepository->get($id);
        $user->notification_id = $input['notification_id'];
        $user->updated_at = Chronos::now()->toIso8601String();

        $userRepository->save($user);

        return $response->withHeader('Content-Type', 'application/json')->write(json_encode([
            'data' => [
                'notification_id' => $user->notification_id
            ]
        ]));
    }
}
