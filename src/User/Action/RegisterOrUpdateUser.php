<?php

namespace FcmResearch\User\Action;

use Cake\Chronos\Chronos;
use FcmResearch\Exception\ValidationException;
use FcmResearch\User\Repository\UserRepository;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class RegisterOrUpdateUser
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function __invoke(Request $request, Response $response)
    {
        $id = $request->getAttribute('user_id');
        $input = $request->getParsedBody()['data'];

        $userRepository = $this->userRepository;

        $username = $input['username'];
        if ($userRepository->isUsernameAvailable($id, $username)) {
            throw new ValidationException('Username already exists.', 'username');
        }

        $user = null;
        if ($userRepository->isExists($id)) {
            $user = $userRepository->get($id);
            $user->username = $username;
            $user->updated_at = Chronos::now()->toIso8601String();
        } else {
            $user = (object) [
                'id' => $id,
                'username' => $input['username'],
                'notification_id' => null,
                'updated_at' => Chronos::now()->toIso8601String()
            ];
        }

        $userRepository->save($user);

        return $response->withHeader('Content-Type', 'application/json')->write(json_encode([
            'data' => [
                'id' => $user->id,
                'username' => $user->username,
                'is_subscribed' => ! is_null($user->notification_id),
                'updated_at' => $user->updated_at
            ]
        ]));
    }
}
