<?php

namespace FcmResearch\User\Repository;

Use Predis\Client;

final class UserRepository
{
    private $redis;

    public function __construct(Client $redis)
    {
        $this->redis = $redis;
    }

    public function findAll(): array
    {
        $redis = $this->redis;

        $rows = $redis->hgetall('fcm_research.usernames');

        $users = [];
        foreach ($rows as $username => $id) {
            $user = $redis->hgetall('fcm_research.users.' . $id);

            $users[] = (object) $user;
        }

        return $users;
    }

    public function isExists(string $id): bool
    {
        $key = 'fcm_research.users.' . $id;

        return $this->redis->hexists($key, 'id') === 1;
    }

    public function get(string $id): \stdClass
    {
        $key = 'fcm_research.users.' . $id;

        return (object) $this->redis->hgetall($key);
    }

    public function save(\stdClass $user)
    {
        $redis = $this->redis;

        $key = 'fcm_research.users.' . $user->id;
        $redis->hset($key, 'id', $user->id);
        $redis->hset($key, 'username', $user->username);
        $redis->hset($key, 'notification_id', $user->notification_id);
        $redis->hset($key, 'updated_at', $user->updated_at);

        $redis->hset('fcm_research.usernames', $user->username, $user->id);
    }

    public function isUsernameAvailable(string $id, string $username): bool
    {
        $existsId = $this->redis->hget('fcm_research.usernames', $username);

        return (! is_null($existsId) && $existsId !== $id);
    }
}
