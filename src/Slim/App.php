<?php

namespace FcmResearch\Slim;

use \DI\ContainerBuilder;
use \Noodlehaus\Config;

final class App extends \DI\Bridge\Slim\App
{
    private $definitionDir;
    private $config;

    public function __construct(string $definitionDir, Config $config, array $container = [])
    {
        $this->definitionDir = $definitionDir;
        $this->config = $config;

        parent::__construct($container);
    }

    protected function configureContainer(ContainerBuilder $builder)
    {
        $config = $this->config;

        $builder->addDefinitions([
            Config::class => $config,
            'config' => $config
        ]);

        $definitions = glob($this->definitionDir . '/*.php');
        foreach ($definitions as $definition) {
            $builder->addDefinitions($definition);
        }
    }
}
