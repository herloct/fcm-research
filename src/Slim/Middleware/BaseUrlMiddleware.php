<?php

namespace FcmResearch\Slim\Middleware;

use Closure;
use FcmResearch\Exception\ValidationException;
use FcmResearch\User\Repository\UserRepository;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class BaseUrlMiddleware
{
    private $baseUrl;

    public function __construct(string $baseUrl)
    {
        $this->baseUrl = $baseUrl;
    }

    public function __invoke(Request $request, Response $response, $next)
    {
        $request = $request->withAttribute('base_url', $this->baseUrl);

        return $next($request, $response);
    }
}
