<?php

namespace FcmResearch\Slim\Handler;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

/**
 * Error handler.
 */
final class Error
{
    /**
     *
     * @var bool
     */
    private $debug;

    /**
     *
     * @var array
     */
    private $maps;

    /**
     *
     * @param type $debug
     * @param array $maps
     */
    public function __construct(
        bool $debug, array $maps
    ) {
        $this->debug = $debug;
        $this->maps = $maps;
    }

    public function __invoke(Request $request, Response $response, $exception)
    {
        $className = get_class($exception);
        $exceptionData = [
            'http_code' => 500,
            'type' => 'unknown-error',
            'message' => $exception->getMessage()
        ];
        if (array_key_exists($className, $this->maps)) {
            $map = $this->maps[$className];
            foreach ($map as $key => $value) {
                if ($value instanceof \Closure) {
                    $exceptionData[$key] = $value($exception);
                } else {
                    $exceptionData[$key] = $value;
                }
            }
        }

        $httpCode = $exceptionData['http_code'];

        $data = [
            'data' => $exceptionData
        ];

        $debug = $this->debug;
        if ($debug) {
            $exceptions = [];

            do {
                $exceptions[] = [
                    'type' => get_class($exception),
                    'code' => $exception->getCode(),
                    'message' => $exception->getMessage(),
                    'file' => $exception->getFile(),
                    'line' => $exception->getLine(),
                    'trace' => explode("\n", $exception->getTraceAsString()),
                ];
            } while ($exception = $exception->getPrevious());

            $data['exceptions'] = $exceptions;
        }

        $body = $response->getBody();
        $body->write(json_encode($data));

        return $response->withStatus($httpCode)
            ->withHeader('Content-Type', 'application/json')
            ->withBody($body);
    }
}
