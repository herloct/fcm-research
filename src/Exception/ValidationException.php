<?php

namespace FcmResearch\Exception;

use Throwable;
use DomainException;

final class ValidationException extends DomainException
{
    private $field;

    public function getField(): string
    {
        return $this->field;
    }

    public function __construct(string $message, string $field, Throwable $previous = null)
    {
        $this->field = $field;

        parent::__construct($message, 400, $previous);
    }

    public function getType(): string
    {
        return 'validation-error';
    }
}
