<?php

namespace FcmResearch\Exception;

use Throwable;
use DomainException;

final class EntityNotFoundException extends DomainException
{
    public function __construct(string $message, Throwable $previous = null)
    {
        parent::__construct($message, 404, $previous);
    }

    public function getType(): string
    {
        return 'not-found';
    }
}
