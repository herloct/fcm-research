<?php

namespace FcmResearch\Template;

use Mustache_Engine;

final class TemplateRenderer
{
    private $engine;

    public function __construct(Mustache_Engine $engine)
    {
        $this->engine = $engine;
    }

    public function render(string $template, $data = []): string
    {
        return $this->engine->render($template, $data);
    }
}
