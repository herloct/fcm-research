<?php

namespace FcmResearch\Notification\Service;

use GuzzleHttp\Client;

final class SendFcmIndividual
{
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function send(string $to, string $title, string $body): \stdClass
    {
        $response = $this->client->post('', [
            'json' => [
                'to' => $to,
                'notification' => [
                    'title' => $title,
                    'body' => $body
                ],

                'priority' => 'high'
            ]
        ]);

        $responseBody = json_decode((string) $response->getBody());

        return $responseBody;
    }
}
