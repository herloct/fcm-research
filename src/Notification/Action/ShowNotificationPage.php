<?php

namespace FcmResearch\Notification\Action;

use Cake\Chronos\Chronos;
use FcmResearch\Template\TemplateRenderer;
use FcmResearch\User\Repository\UserRepository;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Flash\Messages;

final class ShowNotificationPage
{
    private $userRepository;
    private $flash;
    private $templateRenderer;

    public function __construct(UserRepository $userRepository, Messages $flash, TemplateRenderer $templateRenderer)
    {
        $this->userRepository = $userRepository;
        $this->flash = $flash;
        $this->templateRenderer = $templateRenderer;
    }

    public function __invoke(Request $request, Response $response)
    {
        $users = $this->userRepository->findAll();

        $subscribers = [];
        foreach ($users as $user) {
            if (! empty($user->notification_id)) {
                $subscribers[] = $user;
            }
        }

        $flash = $this->flash->getMessages();
        $messages = [
            'success' =>  $flash['success'][0],

            'to' => $flash['to'][0],
            'title' => $flash['title'][0],
            'body' => $flash['body'][0],

            'error_to' => $flash['error_to'][0],
            'error_title' => $flash['error_title'][0],
            'error_body' => $flash['error_body'][0]
        ];

        return $response->withHeader('Content-Type', 'text/html')
            ->write($this->templateRenderer->render('notifications', array_merge([
                'subscribers' => $subscribers
            ], $messages)));
    }
}
