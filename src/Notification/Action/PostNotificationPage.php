<?php

namespace FcmResearch\Notification\Action;

use function Assert\that;
use Assert\AssertionFailedException;
use Exception;
use FcmResearch\Notification\Service\SendFcmIndividual;
use FcmResearch\User\Repository\UserRepository;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Flash\Messages;

final class PostNotificationPage
{
    private $userRepository;
    private $flash;
    private $fcmSender;

    public function __construct(UserRepository $userRepository, Messages $flash, SendFcmIndividual $fcmSender)
    {
        $this->flash = $flash;
        $this->userRepository = $userRepository;
        $this->fcmSender = $fcmSender;
    }

    public function __invoke(Request $request, Response $response)
    {
        $to = $request->getParsedBody()['to'];
        if (is_null($to)) {
            $to = '';
        }

        $title = $request->getParsedBody()['title'];
        $body = $request->getParsedBody()['body'];

        try {
            that($to, null, 'to')
                ->notBlank('Cannot be empty.')
                ->notEmpty('Cannot be empty.')
                ->uuid('Invalid format.');

            that($title, null, 'title')
                ->notNull('Cannot be empty.')
                ->notBlank('Cannot be empty.')
                ->notEmpty('Cannot be empty.');

            that($body, null, 'body')
                ->notNull('Cannot be empty.')
                ->notBlank('Cannot be empty.')
                ->notEmpty('Cannot be empty.');

            $userRepository = $this->userRepository;

            if (! $userRepository->isExists($to)) {
                throw new ValidationException('Cannot find User.', 'to');
            }

            $user = $userRepository->get($to);
            if (empty($user->notification_id)) {
                throw new ValidationException('User does not subscribed to push notification.', 'to');
            }

            $fcmResponse = $this->fcmSender->send($user->notification_id, $title, $body);
            if ($fcmResponse->success === 0) {
                throw new Exception('Notification failed.');
            }

            $this->flash->addMessage('success', 'Notification sent.');
        } catch (AssertionFailedException $e) {
            $this->prepareErrorFlash($to, $title, $body, $e->getPropertyPath(), $e->getMessage());
        } catch (ValidationException $e) {
            $this->prepareErrorFlash($to, $title, $body, $e->getField(), $e->getMessage());
        } catch (Exception $e) {
            $this->prepareErrorFlash($to, $title, $body, 'to', $e->getMessage());
        }

        return $response
            ->withStatus(302)
            ->withHeader('Location', $request->getAttribute('base_url') . '/');
    }

    private function prepareErrorFlash(string $to, string $title, string $body, string $field, string $message)
    {
        $flash = $this->flash;

        $flash->addMessage('to', $to);
        $flash->addMessage('title', $title);
        $flash->addMessage('body', $body);

        $flash->addMessage('error_' . $field, $message);
    }
}
