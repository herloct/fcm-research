# Errors

## 500 Internal Server Error

### Body `application/json`

```yaml
error:
  type: unknown-error
  message: Unknown Error.
```

## 400 Bad Request

### Body `application/json`

```yaml
error:
  type: validation-error
  message: Tidak boleh kosong.
  field: some_field
```
