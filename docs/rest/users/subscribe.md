# Subscribe

Subscribe ke push notification atau update notification_id.

## Request

```
PUT /users/:user_id/notification-id
```

### URI Params

```yaml
user_id:
  type: uuid string
```

### Body `application/json`

```yaml
data:
  type: object
  params:
    notification_id:
      type: string
      desc: notification_id from FCM
```

Example:

```yaml
data:
  notification_id: SOME-NOTIFICATION-ID
```

## Response

### 200 OK

#### Body `application/json`

Example:

```yaml
data:
  notification_id: SOME-NOTIFICATION-ID
```
