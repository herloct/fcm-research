# Unsubscribe

Unsubscribe dari push notification.

## Request

```
DELETE /users/:user_id/notification-id
```

### URI Params

```yaml
user_id:
  type: uuid string
```

## Response

### 200 OK

#### Body `application/json`

Example:

```yaml
data:
  notification_id: null
```
