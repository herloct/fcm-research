# Find All Users

List semua pengguna.

## Request

```
GET /users
```

## Response

### 200 OK

#### Body `application/json`

Example:

```yaml
data:
  - id: 54f84c57-153c-4fd9-b18d-12f7e3826f54
    username: fulan
    updated_at: 2016-09-15T12:14:32+00:00
  - ...
total: 10
```
