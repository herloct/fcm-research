# FCM Research API

## System Requirement

* Docker 1.12.1
* Docker Compose 1.8.0

## How To Setup

1.  Buka CLI pada direktori project.

2.  Nyalakan semua container, dengan menjalankan perintah:

    ```bash
    docker-compose up -d
    ```

    Jika baru pertama kali menjalankan, docker akan mengunduh terlebih dahulu semua image yg diperlukan.

3.  Lakukan `composer install` melalui `docker-compose`, dengan menjalankan perintah:

    ```bash
    docker-compose run --rm composer install
    ```

4.  Jika tidak ada masalah, seharusnya aplikasi sudah dapat diakses melalui:

    ```
    http://domain.com:8081
    ```

    Misal, untuk melihat list pengguna:

    ```
    GET http://domain.com:8081/users
    ```

5.  Jika sudah selesai, sebaiknya matikan semua container, dengan menjalankan perintah:

    ```bash
    docker-compose down
    ```

## Override Docker Container Setting

Silakan buat file `docker-compose.override.yml` jika ingin mengubah (override) setting pada `docker-compose.yml`.

Misalnya apabila ingin menggunakan port `8080`, bisa membuat `docker-compose.override.yml` dengan isi
sebagai berikut:

```yml
version: '2'
services:
  nginx:
    ports:
      - 8080:80
```

Lalu jalankan perintah `docker-compose up -d` seperti biasa.
